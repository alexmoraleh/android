package alex.myprimeraapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ies on 3/12/2018.
 */

public class AgendaSQL extends SQLiteOpenHelper {

    String sqlCreacion ="CREATE TABLE agenda (id integer primary key autoincrement, " +
            "nombre text not null, email text not null);";
    public AgendaSQL (Context context, String name, CursorFactory factory,
                        int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(sqlCreacion);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS Agenda");

        db.execSQL(sqlCreacion);
        // També és podria fer:
        // onCreate(db);
    }

}