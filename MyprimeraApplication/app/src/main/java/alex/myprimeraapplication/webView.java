package alex.myprimeraapplication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


/**
 * Created by ies on 16/11/2018.
 */

public class webView extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        Button bro=(Button) findViewById(R.id.wbbrowser);
        Button call=(Button)findViewById(R.id.wbcall);
        Button cont=(Button)findViewById(R.id.wbcontact);
        Button web=(Button)findViewById(R.id.Webview);
        bro.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com")));
            }
        });
        call.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_DIAL,Uri.parse("tel:+34647344554")));
            }
        });
        cont.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent contacto=new Intent(Intent.ACTION_PICK);
                contacto.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(contacto,1);
            }
        });
        web.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), webView2.class);
                i.setData(Uri.parse("http://www.google.com"));
                startActivity(i);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        //super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==1){
            if (resultCode==RESULT_OK){
                Toast.makeText(getApplicationContext(), data.getData().toString(),Toast.LENGTH_LONG).show();
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(data.getData().toString()));
                startActivity(i);
            }
        }

    }

}