package alex.myprimeraapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Alesito on 23/11/2018.
 */

public class listView extends AppCompatActivity {
    Titular[] datos= new Titular[] {new Titular("Título 1","Subtítulo 1"),
            new Titular("Título 2","Subtítulo 2"),
            new Titular("Título 3","Subtítulo 3"),
            new Titular("Título 4","Subtítulo 4"),
            new Titular("Título 5","Subtítulo 5")};
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        AdaptadorTitulares adaptador = new AdaptadorTitulares(this);

        ListView lstOpciones = (ListView) findViewById(R.id.list1);

        lstOpciones.setAdapter(adaptador);

    }
    class AdaptadorTitulares extends ArrayAdapter {

        Activity context;
        public AdaptadorTitulares(Activity context) {
            super(context, R.layout.activity_listview, datos);
            this.context = (Activity) context;
        }


        // GetView s'executa per cada element de l'array de dades i el que fa
        // es "inflar" el layout del XML que hem creat

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            // return super.getView(position, convertView, parent);
            Log.i("Alesito","1");
            // Inflem el Lauoyt
            LayoutInflater inflater = context.getLayoutInflater();
            // Sobre el layout crear (inflat) dupliquem el layour creat amb els objectes, view personals.
            View item = inflater.inflate(R.layout.activity_listview2, null);
            // OJOOOO!!!!! hemos de hacer el findViewById del item que tenemos inflado.
            TextView lblTitulo = (TextView) item.findViewById(R.id.Titulo);

            lblTitulo.setText(datos[position].getTitulo().toString());
            TextView lblSubTitulo = (TextView) item.findViewById(R.id.Subtitulo);
            // Log.i("Niko","3->"+datosE[position].getSubtitulo().toString() );
            lblSubTitulo.setText(datos[position].getSubtitulo().toString());
            // Log.i("Niko","4");
            return (item);
        }

    }

}
