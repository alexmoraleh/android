package alex.myprimeraapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by ies on 5/10/2018.
 */

public class Formulario extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        final EditText nombre= (EditText) findViewById(R.id.nombre);
        final EditText apellido= (EditText) findViewById(R.id.apellido);
        final EditText email= (EditText) findViewById(R.id.email);
        final EditText fix= (EditText) findViewById(R.id.fijo);
        final EditText movil= (EditText) findViewById(R.id.movil);
        final EditText fecha= (EditText) findViewById(R.id.nacimiento);
        Button validar=(Button) findViewById(R.id.validar);

        View.OnClickListener cl= new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView vw= (TextView)(findViewById(R.id.OK));
                vw.setTextSize(50);
                if(nombre.getText().toString().isEmpty() || apellido.getText().toString().isEmpty() || email.getText().toString().isEmpty() || fix.getText().toString().isEmpty() || movil.getText().toString().isEmpty() || fecha.getText().toString().isEmpty()){
                    vw.setText("KO");
                    vw.setTextColor(Color.RED);
                    vw.setVisibility(View.VISIBLE);
                }
                else{
                    vw.setText("OK");
                    vw.setTextColor(Color.GREEN);
                    nombre.setKeyListener(null);
                    apellido.setKeyListener(null);
                    email.setKeyListener(null);
                    fix.setKeyListener(null);
                    movil.setKeyListener(null);
                    fecha.setKeyListener(null);
                    vw.setVisibility(View.VISIBLE);
                }
            }
        };
        validar.setOnClickListener(cl);


    }
}
