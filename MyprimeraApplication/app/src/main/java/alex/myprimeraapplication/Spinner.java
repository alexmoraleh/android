package alex.myprimeraapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by ies on 9/11/2018.
 */

public class Spinner extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        String[] array = new String[] {"Urgente","No tan urgente","Normal","Mediocre","Pfff"};
        TextView nombre = (TextView) findViewById(R.id.spnombre);
        final TextView email = (TextView) findViewById(R.id.spemail);
        android.widget.Spinner spinner = (android.widget.Spinner) findViewById(R.id.spspinner);
        ArrayAdapter<String> AA= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, array );
        spinner.setAdapter(AA);
        Button enviar = (Button)findViewById(R.id.spenviar);
        final CheckBox check = (CheckBox) findViewById(R.id.spcheck);
        final TextView txt=(TextView)findViewById(R.id.sptexto);
        enviar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");

                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, email.toString());

                if (check.isChecked())
                    emailIntent.putExtra(android.content.Intent.EXTRA_CC, new String[]{});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Probando Spinner");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, txt.toString());
                startActivity(Intent.createChooser
                        (emailIntent, "Enviando correo..."));
            }
        });


    }
}