package alex.myprimeraapplication;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by ies on 19/11/2018.
 */

public class webView2 extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview2);

        Uri url=getIntent().getData();
        WebView webVista= (WebView) findViewById(R.id.webVista);
        webVista.setWebViewClient(new webView2.Callback());
        webVista.loadUrl(url.toString());
    }
    public class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            return(false);
        }
    }

}
