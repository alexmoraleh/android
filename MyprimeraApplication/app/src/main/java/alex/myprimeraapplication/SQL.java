package alex.myprimeraapplication;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by alex on 3/12/2018.
 */

public class SQL extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        AgendaSQL miAgenda = new AgendaSQL(this, "Agenda", null, 1);
        SQLiteDatabase db =  miAgenda.getWritableDatabase();
        if (db != null) {
            String[] s = new String[]{"Alex", "Manuel", "Pepe", "Oriol", "Jesusito"};
            for (int i = 1; i <= 5; i++) {
                String nombre = "Contacto " + s[i];
                String email = s[i] + "elput0amo@gmail.es";

                db.execSQL("INSERT INTO Agenda (nom, email) VALUES ('" + nombre + "', '" + email + "')");
            }
            ContentValues nuevoRegistro =  new ContentValues();
            nuevoRegistro.put("nom", "Joseju");
            nuevoRegistro.put("email", "josejujas@gmail.com");
            db.insert("Agenda", null, nuevoRegistro);
            //linea 124 MainActivity

            Cursor c1 = db.rawQuery("SELECT id, nom, email FROM Agenda WHERE nom='Alex'", null);

            if (c1!= null && c1.moveToFirst()){
                int Id = c1.getInt(0);
                String Nombre = c1.getString(1);
                String Email = c1.getString(2);
            }
            db.close();
            Toast.makeText(this, "Finalizado: GG", Toast.LENGTH_LONG).show();
        }
    }
}
