package alex.myprimeraapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by ies on 8/10/2018.
 */

public class Calculadora extends AppCompatActivity {
    boolean fin=false;
    float result1 = 0;
    float resultfin = 1;
    int operador = 0;
    float memory=0;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        final Button uno=(Button) findViewById(R.id.uno);
        final Button dos=(Button) findViewById(R.id.dos);
        final Button tres=(Button) findViewById(R.id.tres);
        final Button cuatro=(Button) findViewById(R.id.cuatro);
        final Button cinco=(Button) findViewById(R.id.cinco);
        final Button seis=(Button) findViewById(R.id.seis);
        final Button siete=(Button) findViewById(R.id.siete);
        final Button ocho=(Button) findViewById(R.id.ocho);
        final Button nueve=(Button) findViewById(R.id.nueve);
        final Button cero=(Button) findViewById(R.id.cero);
        final Button punto=(Button) findViewById(R.id.punto);
        final Button igual=(Button) findViewById(R.id.igual);
        final Button mas=(Button) findViewById(R.id.mas);
        final Button menos=(Button) findViewById(R.id.menos);
        final Button mult=(Button) findViewById(R.id.multiplicar);
        final Button div=(Button) findViewById(R.id.dividir);
        final Button ms=(Button) findViewById(R.id.ms);
        final Button mr=(Button) findViewById(R.id.mr);
        final Button mmas=(Button) findViewById(R.id.mmas);
        final Button mmenos=(Button) findViewById(R.id.mmenos);
        final Button c=(Button) findViewById(R.id.C);
        final TextView txtvw=(TextView) findViewById(R.id.resultado);



        View.OnClickListener num= new View.OnClickListener(){
            @Override
            public void onClick(View View){
                if(fin){
                    resultfin=1;
                    result1=0;
                    txtvw.setText("");
                    fin=false;
                }
                Button btn = (Button) View;
                txtvw.setText(txtvw.getText()+btn.getText().toString());
                result1 =Float.parseFloat(txtvw.getText().toString());
            }
        };
        View.OnClickListener oper=new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button btn = (Button) view;


                if (btn.getId() == R.id.mas){
                    resultfin =result1+ resultfin;
                    txtvw.setText("");
                    operador =1;
                }
                else {
                    if (btn.getId()== R.id.menos){
                        resultfin=result1-resultfin;
                        txtvw.setText("");
                        operador =2;
                    }
                    else {
                        if (btn.getId()== R.id.multiplicar){
                            resultfin=result1*resultfin;
                            txtvw.setText("");
                            operador =3;
                        }
                        else {
                            if (btn.getId()== R.id.dividir){
                                resultfin=result1/resultfin;
                                txtvw.setText("");
                                operador =4;
                            }
                            else {
                                if (btn.getId()== R.id.igual){
                                    switch(operador){
                                        case 1:
                                            resultfin=(resultfin+result1)-1;
                                            break;
                                        case 2:
                                            resultfin=(resultfin-result1)-1;
                                            break;
                                        case 3:
                                            resultfin=resultfin*result1;
                                            break;
                                        case 4:
                                            resultfin=resultfin/result1;
                                            break;
                                    }
                                    if(resultfin%1==0){
                                        txtvw.setText(""+(int)resultfin);
                                    }
                                    else {
                                        txtvw.setText("" + resultfin);

                                    }
                                    fin = true;
                                }
                            }
                        }
                    }
                }
            }
        };
        View.OnClickListener otro=new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button btn = (Button) view;
                switch (btn.getId()){
                    case R.id.C:
                        txtvw.setText("");
                        result1=0;
                        resultfin=0;
                        break;
                    case R.id.mmas:
                        memory=memory+Float.parseFloat(txtvw.getText().toString());
                        txtvw.setText("");
                        break;
                    case R.id.mmenos:
                        memory=memory-Float.parseFloat(txtvw.getText().toString());
                        txtvw.setText("");
                        break;
                    case R.id.mr:
                        if(memory%1==0){
                            txtvw.setText(""+(int)memory);
                            result1=(int)memory;
                        }
                        else {
                            txtvw.setText("" + memory);
                            result1=memory;
                        }
                        fin=true;
                        break;
                    case R.id.ms:
                        memory=Float.parseFloat(txtvw.getText().toString());
                        txtvw.setText("");
                        break;
                    case R.id.punto:
                        if((txtvw.getText().toString()).contains(".")){

                        }
                        else {
                            txtvw.setText(txtvw.getText() + ".");
                        }
                        break;
                }

            }
        };
        mas.setOnClickListener(oper);
        menos.setOnClickListener(oper);
        igual.setOnClickListener(oper);
        mult.setOnClickListener(oper);
        div.setOnClickListener(oper);
        c.setOnClickListener(otro);
        punto.setOnClickListener(otro);
        mmas.setOnClickListener(otro);
        mmenos.setOnClickListener(otro);
        mr.setOnClickListener(otro);
        ms.setOnClickListener(otro);
        dos.setOnClickListener(num);
        tres.setOnClickListener(num);
        cuatro.setOnClickListener(num);
        cinco.setOnClickListener(num);
        seis.setOnClickListener(num);
        siete.setOnClickListener(num);
        ocho.setOnClickListener(num);
        nueve.setOnClickListener(num);
        cero.setOnClickListener(num);
        uno.setOnClickListener(num);
    }


}
