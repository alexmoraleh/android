package alex.myprimeraapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by ies on 26/10/2018.
 */

public class Doublefirst extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doublefirst);
        final TextView ednombre=(TextView) findViewById(R.id.ednombre);

        final Button button=(Button) findViewById(R.id.button);


        final Toast toast = Toast.makeText(getApplicationContext(), "¡Rellena tu nombre!", Toast.LENGTH_LONG);

        View.OnClickListener verf= new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                if(ednombre.getText().toString().isEmpty()){
                    toast.show();
                }
                else{
                    Intent intent = new Intent(getApplicationContext(), Doublesecond.class);
                    intent.putExtra("Nombre",ednombre.getText().toString());
                    startActivityForResult(intent,1);
                }
            }
        };


        button.setOnClickListener(verf);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1){
            if(resultCode==RESULT_OK){
                findViewById(R.id.progreso).setVisibility(View.GONE);
                findViewById(R.id.finish).setVisibility(View.VISIBLE);
                final TextView ednombre=(TextView) findViewById(R.id.ednombre);
                final TextView finish=(TextView)findViewById(R.id.finish);
                ednombre.setText("");
                finish.setText(data.getStringExtra("result"));
                if(finish.getText().toString().equals("Aceptado"))
                    finish.setTextColor(Color.GREEN);

                else finish.setTextColor(Color.RED);


            }
        }
    }
}
