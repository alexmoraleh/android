package alex.myprimeraapplication;

/**
 * Created by Alesito on 23/11/2018.
 */

public class Titular {
    private String titulo;
    private String subtitulo;

    public Titular(String tit, String sub) {
        titulo=tit;
        subtitulo=sub;
    }

    public String getTitulo(){
        return titulo;
    }

    public String getSubtitulo(){
        return subtitulo;
    }

}
