package alex.myprimeraapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by ies on 5/10/2018.
 */

public class ContadorLet extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contador);

        //getWindow().setTitle(getResources());

        View.OnClickListener c1= new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText textedit=(EditText) findViewById(R.id.insertar);
                String txt= textedit.getText().toString();

                TextView txtview = (TextView) findViewById(R.id.mostrar);
                txtview.setText("Hay "+txt.length()+" carácteres.");
            }
        };
        Button btn=(Button) findViewById(R.id.btn);
        btn.setOnClickListener(c1);

        EditText edittxt = (EditText) findViewById(R.id.insertar);
        edittxt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent){
                TextView txtvw = (TextView) findViewById(R.id.mostrar);
                txtvw.setText(getResources().getText(R.string.vacio));
                return false;
            }
        });
        //edittxt.setOnClickListener();
    }
}
