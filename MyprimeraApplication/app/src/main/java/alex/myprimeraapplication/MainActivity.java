package alex.myprimeraapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView invisibletxt = (TextView) findViewById(R.id.invisibletxt);
        final Button visiblebtn = (Button) findViewById(R.id.visiblebtn);
        final Button visible2btn = (Button) findViewById(R.id.visible2btn);
        //if(visible2btn.getVisibility()!=View.VISIBLE)
        visiblebtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                invisibletxt.setVisibility(View.INVISIBLE);
                visiblebtn.setVisibility(View.GONE);
                visible2btn.setVisibility(View.VISIBLE);
            }
        });
        visible2btn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                invisibletxt.setVisibility(View.VISIBLE);
                visiblebtn.setVisibility(View.VISIBLE);
                visible2btn.setVisibility(View.GONE);
            }
        });


    }


}
