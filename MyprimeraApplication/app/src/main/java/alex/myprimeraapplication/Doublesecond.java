package alex.myprimeraapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by ies on 29/10/2018.
 */

public class Doublesecond extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doublesecond);

        TextView txt=(TextView) findViewById(R.id.textosecond);
        Bundle bundle =this.getIntent().getExtras();
        txt.setText("Hola "+(String)bundle.get("Nombre")+", ¿Acepta los terminos y condiciones?");
        Button acept=(Button) findViewById(R.id.si);
        Button decl=(Button) findViewById(R.id.no);
        acept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.putExtra("result", "Aceptado");
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        decl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.putExtra("result", "No aceptado");
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
