package orilex.proyecto;

/**
 * Created by ies on 14/1/2019.
 */

public class Listas {
    private String titulo;
    private String subtitulo;

    public Listas (String tit, String sub){
        titulo = tit;
        subtitulo=sub;
    }
    public Listas (String tit){
        titulo=tit;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getSubtitulo() {
        return subtitulo;
    }
}
