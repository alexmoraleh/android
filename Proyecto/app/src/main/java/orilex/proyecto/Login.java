package orilex.proyecto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ies on 18/1/2019.
 */

public class Login extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        TextView user=(TextView)findViewById(R.id.user);
        TextView pass=(TextView)findViewById(R.id.pass);
        Button log=(Button)findViewById(R.id.login);

        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Comprobar si usuario/contraseña coinciden en la bd

                Intent intent= new Intent(getApplicationContext(),Inicio.class);
                intent.putExtra("Mete algo aqui","Aqui tambien");
                startActivity(intent);
                finish();
            }
        });
    }


}