package orilex.proyecto;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by ies on 9/1/2019.
 */

public class Inicio extends AppCompatActivity {
    Listas[] datos= new Listas[] {new Listas("Título 1","Subtítulo 1"), //Esto es para ver algo, sustituir luego por los datos de la bdd
            new Listas("Título 2","Subtítulo 2"),
            new Listas("Título 3","Subtítulo 3"),
            new Listas("Título 4","Subtítulo 4"),
            new Listas("Título 5","Subtítulo 5")};
    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Menu header= (Menu) getMenuInflater().inflate(R.menu.main);
        /*Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar); //Todo esto es para la flechita de ir para atras
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.ic_person_black_24dp);
        //mToolbar.setLogo(R.drawable.ic_action_name);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            //https://www.codingdemos.com/android-toolbar-back-button/
            @Override
            public void onClick(View view) {
                finish();//Cambiar esto para que vuelva a la activity de antes
            }
        });*/
        final Toast toast = Toast.makeText(getApplicationContext(), "Login satisfactorio", Toast.LENGTH_LONG);
        toast.show();

        //menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_search_black_24dp));
        //MenuItem item_user = (MenuItem) R.id.item_user;


        Adaptador adaptador = new Adaptador(this);
        ListView comandas = (ListView) findViewById(R.id.comandas);
        comandas.setAdapter(adaptador);
        comandas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent= new Intent(getApplicationContext(),Comanda.class);
                intent.putExtra("Mete algo aqui","Aqui tambien");//Pasar el titulo del producto y la fecha
                startActivity(intent);
                //finish(); no hay que hacer finish, para poder volver
            }
        });


    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem settingsItem = menu.findItem(R.id.item_user);
        // set your desired icon here based on a flag if you like
        settingsItem.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_search_black_24dp));// Meter en drawable las imagenes de usuario y ya ahi ir haciendo


        return super.onPrepareOptionsMenu(menu);
    }
    class Adaptador extends ArrayAdapter {

        Activity context;
        public Adaptador (Activity context) {
            super(context, R.layout.activity_main, datos); //datos es un array con los datos que poner en la lista
            this.context = (Activity) context;
        }


        // GetView s'executa per cada element de l'array de dades i el que fa
        // es "inflar" el layout del XML que hem creat

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            // return super.getView(position, convertView, parent);
            Log.i("Alesito","1");
            // Inflem el Lauoyt
            LayoutInflater inflater = context.getLayoutInflater();
            // Sobre el layout crear (inflat) dupliquem el layour creat amb els objectes, view personals.
            View item = inflater.inflate(R.layout.listview, null);
            // OJOOOO!!!!! hemos de hacer el findViewById del item que tenemos inflado.
            TextView lblTitulo = (TextView) item.findViewById(R.id.Titulo);

            lblTitulo.setText(datos[position].getTitulo().toString());
            TextView lblSubTitulo = (TextView) item.findViewById(R.id.Subtitulo);
            // Log.i("Niko","3->"+datosE[position].getSubtitulo().toString() );
            lblSubTitulo.setText(datos[position].getSubtitulo().toString());
            // Log.i("Niko","4");
            return (item);
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        this.menu=menu;
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        if (item.getItemId() == R.id.item_user) {
            Intent intent=new Intent(getApplicationContext(), User.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

}
