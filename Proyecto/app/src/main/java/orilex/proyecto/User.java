package orilex.proyecto;

import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ies on 17/12/2018.
 */

public class User extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user);
        //Aqui habia un boton para editar el usuario (pereza)
        Button out=(Button)findViewById(R.id.usrout);
        TextView name=(TextView)findViewById(R.id.usrname);
        TextView email=(TextView)findViewById(R.id.usremail);
        ImageView img=(ImageView) findViewById(R.id.usrimage);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            //https://www.codingdemos.com/android-toolbar-back-button/
            @Override
            public void onClick(View view) {
                finish();//Cambiar esto para que vuelva a la activity de antes
            }
        });
        //Meter acciones para cada view
    }


}
